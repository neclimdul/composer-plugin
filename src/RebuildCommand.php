<?php

namespace SpoonsPlugin;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class RebuildCommand extends BaseCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this->setName('drupalspoons:rebuild');
    $this->setDescription('Rebuild codebase via a re-install of composer-plugin.');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $composerJson = getenv('COMPOSER');
    $composerLock = str_replace('.json', '.lock', $composerJson);
    $fs = new Filesystem();
    $script = $fs->tempnam(getcwd(), 'drupalspoons-setup-');
    $fs->remove($script);
    $fs->copy('vendor/drupalspoons/composer-plugin/bin/setup', $script);
    $fs->chmod($script, 0755);
    foreach ([$composerJson, $composerLock, 'vendor', 'web'] as $item) {
      $fs->remove($item);
    }
    passthru($script, $exit_code);
    $fs->remove($script);
    return $exit_code;
  }

}
